import { HardhatUserConfig } from 'hardhat/config';
// import "@nomicfoundation/hardhat-toolbox";
import '@nomiclabs/hardhat-etherscan';
import '@nomiclabs/hardhat-waffle';
import '@typechain/hardhat';
import 'hardhat-gas-reporter';
import 'solidity-coverage';

import * as dotenv from 'dotenv';

// Tasks
// require('./tasks/<name>');

dotenv.config();

const config: HardhatUserConfig = {
    solidity: '0.8.15',
    
    paths: {
        sources: './contracts',
        tests: './test',
        cache: './cache',
        artifacts: './artifacts'
    },

    networks: {
        hardhat: {},

        goerli: {
            url: process.env.CL_ETH_GOERLI_RPC || '',
            accounts: process.env.CL_DEV_EOA_PRIV
                ? [process.env.CL_DEV_EOA_PRIV]
                : [],
            // gasPrice: 30000000000,
        },

        ethereum: {},
    },

    etherscan: {
        apiKey: {
            goerli: process.env.CL_ETHERSCAN_API_KEY
                ? process.env.CL_ETHERSCAN_API_KEY
                : ''
        },
    },

    gasReporter: {
        enabled: process.env.REPORT_GAS !== undefined,
        currency: 'USD',
    },
};
